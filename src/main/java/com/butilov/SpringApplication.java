package com.butilov;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringApplication {

    private SpringApplication() {
        new AnnotationConfigApplicationContext(SpringConfiguration.class);
    }

    public static void main(String[] args) {
        new SpringApplication();
    }
}